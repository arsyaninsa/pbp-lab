from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from lab_2.models import Note
from lab_4.forms import NoteForm

# Create your views here.
def index(request):
    list_note = Note.objects.all()
    response = {'list_note' : list_note}
    return render(request, 'lab4_index.html', response )

def note_list(request):
    list_note = Note.objects.all()
    response = {'list_note' : list_note}
    return render(request, 'lab4_note_list.html', response )

def add_note(request):   
    
    #check POST request not get request
    if request.method == 'POST':

        #initiate form wh FriendFOrm
        form = NoteForm(request.POST)

        #Friend valid
        if form.is_valid():
            form.save()
            return HttpResponseRedirect ('/lab4/')
    
    
    else:
        form = NoteForm()    

        
    return render(request, 'lab4_form.html', {'form': form})