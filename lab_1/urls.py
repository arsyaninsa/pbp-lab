from django.urls import path
#import method dari views supaya bisa dipake di urls
from .views import index,friend_list

urlpatterns = [
    path('', index, name='index'), #lab1/
    # TODO Add friends path using friend_list Views
    #path friend 
    path('friends/', friend_list, name='friend'),
]
