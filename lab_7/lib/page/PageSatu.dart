import 'package:flutter/material.dart';

class PageSatu extends StatelessWidget{
  final _myKey = GlobalKey<FormState>();
  String formValue = "";
  final TextEditingController myController = TextEditingController();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[900],
        title: Text("E - Nadi"),
      ),

      body: Center(
        child : Padding(
          padding: const EdgeInsets.all(25),
          child: Form(
            key : _myKey,
            child:Column(
                children:[
                  TextFormField(
                    showCursor: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.black,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.black,
                        ),
                      ),

                      hintText: "Enter your feedback",


                    ),

                    validator: (value){
                      if (value!.isEmpty){
                        return 'please fill the feedback';
                      }
                      formValue= value;
                      return null;
                    },

                  ),

                  ElevatedButton(
                    onPressed: (){
                      if (_myKey.currentState!.validate()){
                        print(formValue);
                      }

                    },

                    child: const Text('Submit'),
                  ),
                ]
            ),
          ),
        ),
      ),
    );
  }
}