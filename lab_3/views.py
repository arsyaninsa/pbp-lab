from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_1.models import Friend 
from .forms import FriendForm
from django import forms
from django.contrib.auth.decorators import login_required



# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
    # list_note = Note.objects.all()
    # response = {'list_note' : list_note}
    list_friends = Friend.objects.all() # for listing all Friend Object
    response = {'list_friend': list_friends} #friends : Arsyan
    return render(request, 'friend_list_lab1.html', response)

@login_required(login_url="/admin/login/")
def add_friend(request):   
    
    #check POST request not get request
    if request.method == 'POST':

        #initiate form wh FriendFOrm
        form = FriendForm(request.POST)

        #Friend valid
        if form.is_valid():
            form.save()
            return HttpResponseRedirect ('/lab3/')
    
    
    else:
        form = FriendForm()    

        
    return render(request, 'lab3_form.html', {'form': form})





    
