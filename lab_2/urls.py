from django.urls import path

from .views import index, xml, json

urlpatterns = [
    path('', index, name='index'), #lab2
    path('xml', xml, name='xml' ),
    path('json', json, name = 'json')   
    
]