# Generated by Django 3.2.7 on 2021-09-27 01:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0002_rename_recepient_note_sender'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='message',
            field=models.TextField(),
        ),
    ]
