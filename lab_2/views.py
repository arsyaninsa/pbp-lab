from django.shortcuts import render
from .models import Note
# Import HttpResponse from django.http.response
from django.http.response import HttpResponse
#Import serializers from django.core below import in step 6.1.
from django.core import serializers

def index(request):
    list_note = Note.objects.all()
    response = {'list_note' : list_note}
    return render(request, 'lab2.html', response )

def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")


# Create your views here.
