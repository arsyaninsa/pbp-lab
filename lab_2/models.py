from django.db import models

# Create your models here.
class Note(models.Model):
    title = models.CharField(max_length=30)
    to = models.CharField(max_length=30)
    sender = models.CharField(max_length=30)
    message = models.TextField()
    # TODO Implement missing attributes in Friend model


    
